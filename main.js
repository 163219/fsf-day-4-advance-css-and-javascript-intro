//load express
var express = require ("express");

//Create an application
var app = express ();

//Define my document root
//static resources will be served form here
app.use(express.static (__dirname + "/public") );

app.listen(3000, function(){
    console.info("Application is listening on port 3000");
});

